package com.afpa.service;

import com.afpa.access.BdAccess;
import com.afpa.model.Utilisateur;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named
@SessionScoped
public class ServiceUtilisateur implements Serializable {


    @Inject
    BdAccess bda;

    Utilisateur utilisateur;

    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    @PostConstruct
    public void init(){
        utilisateur = new Utilisateur();
    }


    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    public BdAccess getBda() {
        return bda;
    }

    public void setBda(BdAccess bda) {
        this.bda = bda;
    }


    public List<Utilisateur> listeUtilisateurs(){
        return bda.lireallutilisateurs(); // récupérer les utilisateurs depuis la base est les mettres dans la liste

    }

    public void ajouterUtilisateur(){
        utilisateur.setId(1); // le 1er utilisateur
        utilisateur.setPrivilege("users");
        bda.write(utilisateur);
    }

}
