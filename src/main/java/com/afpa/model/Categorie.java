package com.afpa.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "categorie")
public class Categorie implements Entities, Serializable {
    private int id;
    private String libelle;
    private Collection<Produit> produitsById;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy= GenerationType.IDENTITY)//Clé primaire auto-incrémenté
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "libelle")
    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Categorie categorie = (Categorie) o;
        return id == categorie.id &&
                Objects.equals(libelle, categorie.libelle);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, libelle);
    }

    @OneToMany(mappedBy = "categorieByIdCategorie")
    public Collection<Produit> getProduitsById() {
        return produitsById;
    }

    public void setProduitsById(Collection<Produit> produitsById) {
        this.produitsById = produitsById;
    }
}
