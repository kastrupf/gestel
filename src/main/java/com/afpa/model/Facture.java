package com.afpa.model;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "facture")
public class Facture implements Entities, Serializable {
    private int idFac;
    private String numero;
    private Date datefacture;
    private byte etat;
    private int id;
    private int idClient;
    private Collection<Commande> commandesByIdFac;
    private Utilisateur utilisateurById;
    private Client clientByIdClient;

    @Id
    @Column(name = "idFac")
    @GeneratedValue(strategy= GenerationType.IDENTITY)//Clé primaire auto-incrémenté
    public int getIdFac() {
        return idFac;
    }

    public void setIdFac(int idFac) {
        this.idFac = idFac;
    }

    @Basic
    @Column(name = "numero")
    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    @Basic
    @Column(name = "datefacture")
    public Date getDatefacture() {
        return datefacture;
    }

    public void setDatefacture(Date datefacture) {
        this.datefacture = datefacture;
    }

    @Basic
    @Column(name = "etat")
    public byte getEtat() {
        return etat;
    }

    public void setEtat(byte etat) {
        this.etat = etat;
    }

    @Basic
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "id_CLIENT")
    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Facture facture = (Facture) o;
        return idFac == facture.idFac &&
                etat == facture.etat &&
                id == facture.id &&
                idClient == facture.idClient &&
                Objects.equals(numero, facture.numero) &&
                Objects.equals(datefacture, facture.datefacture);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idFac, numero, datefacture, etat, id, idClient);
    }

    @OneToMany(mappedBy = "factureByIdFac")
    public Collection<Commande> getCommandesByIdFac() {
        return commandesByIdFac;
    }

    public void setCommandesByIdFac(Collection<Commande> commandesByIdFac) {
        this.commandesByIdFac = commandesByIdFac;
    }

    @ManyToOne
    @PrimaryKeyJoinColumn(name = "id", referencedColumnName = "id")
    public Utilisateur getUtilisateurById() {
        return utilisateurById;
    }

    public void setUtilisateurById(Utilisateur utilisateurById) {
        this.utilisateurById = utilisateurById;
    }

    @ManyToOne
    @PrimaryKeyJoinColumn(name = "id_CLIENT", referencedColumnName = "id")
    public Client getClientByIdClient() {
        return clientByIdClient;
    }

    public void setClientByIdClient(Client clientByIdClient) {
        this.clientByIdClient = clientByIdClient;
    }
}
