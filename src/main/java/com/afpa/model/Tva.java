package com.afpa.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "tva")
public class Tva implements Entities, Serializable {
    private int id;
    private int taux;
    private Collection<Produit> produitsById;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy= GenerationType.IDENTITY)//Clé primaire auto-incrémenté
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "taux")
    public int getTaux() {
        return taux;
    }

    public void setTaux(int taux) {
        this.taux = taux;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tva tva = (Tva) o;
        return id == tva.id &&
                taux == tva.taux;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, taux);
    }

    @OneToMany(mappedBy = "tvaById")
    public Collection<Produit> getProduitsById() {
        return produitsById;
    }

    public void setProduitsById(Collection<Produit> produitsById) {
        this.produitsById = produitsById;
    }
}
