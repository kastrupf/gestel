package com.afpa.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "client")
public class Client implements Entities, Serializable {
    private int id;
    private String nom;
    private String prenom;
    private String adresse;
    private String cp;
    private String commune;
    private String telephone;
    private String email;
    private Collection<Devis> devisById;
    private Collection<Facture> facturesById;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy= GenerationType.IDENTITY)//Clé primaire auto-incrémenté
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nom")
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Basic
    @Column(name = "prenom")
    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    @Basic
    @Column(name = "adresse")
    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    @Basic
    @Column(name = "cp")
    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    @Basic
    @Column(name = "commune")
    public String getCommune() {
        return commune;
    }

    public void setCommune(String commune) {
        this.commune = commune;
    }

    @Basic
    @Column(name = "telephone")
    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return id == client.id &&
                Objects.equals(nom, client.nom) &&
                Objects.equals(prenom, client.prenom) &&
                Objects.equals(adresse, client.adresse) &&
                Objects.equals(cp, client.cp) &&
                Objects.equals(commune, client.commune) &&
                Objects.equals(telephone, client.telephone) &&
                Objects.equals(email, client.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nom, prenom, adresse, cp, commune, telephone, email);
    }

    @OneToMany(mappedBy = "clientById")
    public Collection<Devis> getDevisById() {
        return devisById;
    }

    public void setDevisById(Collection<Devis> devisById) {
        this.devisById = devisById;
    }

    @OneToMany(mappedBy = "clientByIdClient")
    public Collection<Facture> getFacturesById() {
        return facturesById;
    }

    public void setFacturesById(Collection<Facture> facturesById) {
        this.facturesById = facturesById;
    }
}
