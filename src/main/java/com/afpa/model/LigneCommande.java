package com.afpa.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "ligne_commande", schema = "projet", catalog = "")
public class LigneCommande implements Entities, Serializable {
    private int idProd;
    private int idCom;
    private int qte;
    private int remise;
    private Produit produitByIdProd;
    private Commande commandeByIdCom;

    @Id
    @Column(name = "idProd")
    public int getIdProd() {
        return idProd;
    }

    public void setIdProd(int idProd) {
        this.idProd = idProd;
    }

    @Id
    @Column(name = "idCom")
    public int getIdCom() {
        return idCom;
    }

    public void setIdCom(int idCom) {
        this.idCom = idCom;
    }

    @Basic
    @Column(name = "qte")
    public int getQte() {
        return qte;
    }

    public void setQte(int qte) {
        this.qte = qte;
    }

    @Basic
    @Column(name = "remise")
    public int getRemise() {
        return remise;
    }

    public void setRemise(int remise) {
        this.remise = remise;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LigneCommande that = (LigneCommande) o;
        return idProd == that.idProd &&
                idCom == that.idCom &&
                qte == that.qte &&
                remise == that.remise;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idProd, idCom, qte, remise);
    }

    @ManyToOne
    @PrimaryKeyJoinColumn(name = "idProd", referencedColumnName = "idProd")
    public Produit getProduitByIdProd() {
        return produitByIdProd;
    }

    public void setProduitByIdProd(Produit produitByIdProd) {
        this.produitByIdProd = produitByIdProd;
    }

    @ManyToOne
    @PrimaryKeyJoinColumn(name = "idCom", referencedColumnName = "idCom")
    public Commande getCommandeByIdCom() {
        return commandeByIdCom;
    }

    public void setCommandeByIdCom(Commande commandeByIdCom) {
        this.commandeByIdCom = commandeByIdCom;
    }
}
