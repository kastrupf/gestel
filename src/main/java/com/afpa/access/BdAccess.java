package com.afpa.access;

import com.afpa.model.Categorie;
import com.afpa.model.Entities;
import com.afpa.model.Produit;
import com.afpa.model.Utilisateur;
import com.google.common.hash.Hashing;
import javax.faces.view.ViewScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.List;


@ViewScoped
public class BdAccess implements Serializable {

   @PersistenceContext(unitName = "GestComPersistenceUnit")
    private EntityManager em;

    public BdAccess() {

    }

    public List<Utilisateur> lireallutilisateurs() {
        return em.createNativeQuery("select * from UTILISATEUR", Utilisateur.class).getResultList();
    }

    public List<Produit> lireallproduit() {
        return em.createNativeQuery("select * from PRODUIT", Produit.class).getResultList();
    }

    public List<Categorie> lireallcategorie() {
        return em.createNativeQuery("select * from CATEGORIE", Categorie.class).getResultList();
    }

    @Transactional
    public void write(Entities e) {
        em.persist(e);
    }

    @Transactional
    public boolean write(Utilisateur u)
    {
        u.setMotdepasse(Hashing.sha256()
                .hashString(u.getMotdepasse(), StandardCharsets.UTF_8)
                .toString());
        em.persist(u);
        return em.contains(u);
    }

    @Transactional
    public void refresh(Entities c)
    {
        if(!em.contains(c)) //t n'est pas managé
        {
            c=em.merge(c); //manage t
        }
        em.refresh(c);
    }

    public EntityManager getEm() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }
}


