package com.afpa.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Produit {
    private int idProd;
    private String reference;
    private String descriptif;
    private int prixUnitaire;

    @Id
    @Column(name = "idProd", nullable = false)
    public int getIdProd() {
        return idProd;
    }

    public void setIdProd(int idProd) {
        this.idProd = idProd;
    }

    @Basic
    @Column(name = "reference", nullable = false, length = 50)
    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    @Basic
    @Column(name = "descriptif", nullable = true, length = 50)
    public String getDescriptif() {
        return descriptif;
    }

    public void setDescriptif(String descriptif) {
        this.descriptif = descriptif;
    }

    @Basic
    @Column(name = "prixUnitaire", nullable = false, precision = 0)
    public int getPrixUnitaire() {
        return prixUnitaire;
    }

    public void setPrixUnitaire(int prixUnitaire) {
        this.prixUnitaire = prixUnitaire;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Produit produit = (Produit) o;
        return idProd == produit.idProd &&
                prixUnitaire == produit.prixUnitaire &&
                Objects.equals(reference, produit.reference) &&
                Objects.equals(descriptif, produit.descriptif);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idProd, reference, descriptif, prixUnitaire);
    }
}
