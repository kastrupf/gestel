package com.afpa.service;

import com.afpa.access.BdAccess;
import com.afpa.model.Categorie;
import com.afpa.model.Produit;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named
@SessionScoped
public class ServiceProduit  implements Serializable {

    @Inject
    BdAccess bda;

    Produit produit;

    public Produit getProduit() {
        return produit;
    }

    @PostConstruct
    public void init(){
        produit = new Produit();
    }


    public void setProduit(Produit produit) {
        this.produit = produit;
    }

    public BdAccess getBda() {
        return bda;
    }

    public void setBda(BdAccess bda) {
        this.bda = bda;
    }


    public  List<Produit> listeProduits(){
        return bda.lireallproduit(); // récupérer les produits depuis la base est les mettres dans la liste

    }

    public  List<Categorie> listeCategories(){
        return bda.lireallcategorie(); // récupérer les catégories depuis la base est les mettres dans la liste

    }

    public void ajouterProduit(){
        produit.setId(1); // la 1er tva
        produit.setIdCategorie(1);
        bda.write(produit);
    }

}
