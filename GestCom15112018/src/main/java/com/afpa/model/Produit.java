package com.afpa.model;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
public class Produit {
    private int idProd;
    private String reference;
    private String descriptif;
    private int prixUnitaire;
    private int id;
    private int idCategorie;
    private Tva tvaById;
    private Categorie categorieByIdCategorie;
    private Collection<LigneCommande> ligneCommandesByIdProd;
    private Collection<LigneDevis> ligneDevisByIdProd;

    @Id
    @Column(name = "idProd")
    public int getIdProd() {
        return idProd;
    }

    public void setIdProd(int idProd) {
        this.idProd = idProd;
    }

    @Basic
    @Column(name = "reference")
    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    @Basic
    @Column(name = "descriptif")
    public String getDescriptif() {
        return descriptif;
    }

    public void setDescriptif(String descriptif) {
        this.descriptif = descriptif;
    }

    @Basic
    @Column(name = "prixUnitaire")
    public int getPrixUnitaire() {
        return prixUnitaire;
    }

    public void setPrixUnitaire(int prixUnitaire) {
        this.prixUnitaire = prixUnitaire;
    }

    @Basic
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "id_CATEGORIE")
    public int getIdCategorie() {
        return idCategorie;
    }

    public void setIdCategorie(int idCategorie) {
        this.idCategorie = idCategorie;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Produit produit = (Produit) o;
        return idProd == produit.idProd &&
                prixUnitaire == produit.prixUnitaire &&
                id == produit.id &&
                idCategorie == produit.idCategorie &&
                Objects.equals(reference, produit.reference) &&
                Objects.equals(descriptif, produit.descriptif);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idProd, reference, descriptif, prixUnitaire, id, idCategorie);
    }

    @ManyToOne
    @PrimaryKeyJoinColumn(name = "id", referencedColumnName = "id")
    public Tva getTvaById() {
        return tvaById;
    }

    public void setTvaById(Tva tvaById) {
        this.tvaById = tvaById;
    }

    @ManyToOne
    @PrimaryKeyJoinColumn(name = "id_CATEGORIE", referencedColumnName = "id")
    public Categorie getCategorieByIdCategorie() {
        return categorieByIdCategorie;
    }

    public void setCategorieByIdCategorie(Categorie categorieByIdCategorie) {
        this.categorieByIdCategorie = categorieByIdCategorie;
    }

    @OneToMany(mappedBy = "produitByIdProd")
    public Collection<LigneCommande> getLigneCommandesByIdProd() {
        return ligneCommandesByIdProd;
    }

    public void setLigneCommandesByIdProd(Collection<LigneCommande> ligneCommandesByIdProd) {
        this.ligneCommandesByIdProd = ligneCommandesByIdProd;
    }

    @OneToMany(mappedBy = "produitByIdProd")
    public Collection<LigneDevis> getLigneDevisByIdProd() {
        return ligneDevisByIdProd;
    }

    public void setLigneDevisByIdProd(Collection<LigneDevis> ligneDevisByIdProd) {
        this.ligneDevisByIdProd = ligneDevisByIdProd;
    }
}
