package com.afpa.model;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
public class Utilisateur {
    private int id;
    private String pseudo;
    private String nom;
    private String prenom;
    private String motdepasse;
    private String mail;
    private String privilege;
    private Collection<Devis> devisById;
    private Collection<Facture> facturesById;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "pseudo")
    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    @Basic
    @Column(name = "nom")
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Basic
    @Column(name = "prenom")
    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    @Basic
    @Column(name = "motdepasse")
    public String getMotdepasse() {
        return motdepasse;
    }

    public void setMotdepasse(String motdepasse) {
        this.motdepasse = motdepasse;
    }

    @Basic
    @Column(name = "mail")
    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    @Basic
    @Column(name = "privilege")
    public String getPrivilege() {
        return privilege;
    }

    public void setPrivilege(String privilege) {
        this.privilege = privilege;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Utilisateur that = (Utilisateur) o;
        return id == that.id &&
                Objects.equals(pseudo, that.pseudo) &&
                Objects.equals(nom, that.nom) &&
                Objects.equals(prenom, that.prenom) &&
                Objects.equals(motdepasse, that.motdepasse) &&
                Objects.equals(mail, that.mail) &&
                Objects.equals(privilege, that.privilege);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, pseudo, nom, prenom, motdepasse, mail, privilege);
    }

    @OneToMany(mappedBy = "utilisateurByIdUtilisateur")
    public Collection<Devis> getDevisById() {
        return devisById;
    }

    public void setDevisById(Collection<Devis> devisById) {
        this.devisById = devisById;
    }

    @OneToMany(mappedBy = "utilisateurById")
    public Collection<Facture> getFacturesById() {
        return facturesById;
    }

    public void setFacturesById(Collection<Facture> facturesById) {
        this.facturesById = facturesById;
    }
}
