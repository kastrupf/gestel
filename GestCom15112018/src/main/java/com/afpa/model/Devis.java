package com.afpa.model;

import javax.persistence.*;
import java.sql.Date;
import java.util.Collection;
import java.util.Objects;

@Entity
public class Devis {
    private int idDev;
    private String numero;
    private Date datedudevis;
    private int id;
    private int idUtilisateur;
    private Collection<Commande> commandesByIdDev;
    private Client clientById;
    private Utilisateur utilisateurByIdUtilisateur;
    private Collection<LigneDevis> ligneDevisByIdDev;

    @Id
    @Column(name = "idDev")
    public int getIdDev() {
        return idDev;
    }

    public void setIdDev(int idDev) {
        this.idDev = idDev;
    }

    @Basic
    @Column(name = "numero")
    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    @Basic
    @Column(name = "datedudevis")
    public Date getDatedudevis() {
        return datedudevis;
    }

    public void setDatedudevis(Date datedudevis) {
        this.datedudevis = datedudevis;
    }

    @Basic
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "id_UTILISATEUR")
    public int getIdUtilisateur() {
        return idUtilisateur;
    }

    public void setIdUtilisateur(int idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Devis devis = (Devis) o;
        return idDev == devis.idDev &&
                id == devis.id &&
                idUtilisateur == devis.idUtilisateur &&
                Objects.equals(numero, devis.numero) &&
                Objects.equals(datedudevis, devis.datedudevis);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idDev, numero, datedudevis, id, idUtilisateur);
    }

    @OneToMany(mappedBy = "devisByIdDev")
    public Collection<Commande> getCommandesByIdDev() {
        return commandesByIdDev;
    }

    public void setCommandesByIdDev(Collection<Commande> commandesByIdDev) {
        this.commandesByIdDev = commandesByIdDev;
    }

    @ManyToOne
    @PrimaryKeyJoinColumn(name = "id", referencedColumnName = "id")
    public Client getClientById() {
        return clientById;
    }

    public void setClientById(Client clientById) {
        this.clientById = clientById;
    }

    @ManyToOne
    @PrimaryKeyJoinColumn(name = "id_UTILISATEUR", referencedColumnName = "id")
    public Utilisateur getUtilisateurByIdUtilisateur() {
        return utilisateurByIdUtilisateur;
    }

    public void setUtilisateurByIdUtilisateur(Utilisateur utilisateurByIdUtilisateur) {
        this.utilisateurByIdUtilisateur = utilisateurByIdUtilisateur;
    }

    @OneToMany(mappedBy = "devisByIdDev")
    public Collection<LigneDevis> getLigneDevisByIdDev() {
        return ligneDevisByIdDev;
    }

    public void setLigneDevisByIdDev(Collection<LigneDevis> ligneDevisByIdDev) {
        this.ligneDevisByIdDev = ligneDevisByIdDev;
    }
}
