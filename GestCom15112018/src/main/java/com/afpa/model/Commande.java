package com.afpa.model;

import javax.persistence.*;
import java.sql.Date;
import java.util.Collection;
import java.util.Objects;

@Entity
public class Commande {
    private int idCom;
    private String numero;
    private byte etat;
    private Date datecommande;
    private Date datelivraison;
    private int idDev;
    private Integer idFac;
    private Devis devisByIdDev;
    private Facture factureByIdFac;
    private Collection<LigneCommande> ligneCommandesByIdCom;

    @Id
    @Column(name = "idCom")
    public int getIdCom() {
        return idCom;
    }

    public void setIdCom(int idCom) {
        this.idCom = idCom;
    }

    @Basic
    @Column(name = "numero")
    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    @Basic
    @Column(name = "etat")
    public byte getEtat() {
        return etat;
    }

    public void setEtat(byte etat) {
        this.etat = etat;
    }

    @Basic
    @Column(name = "datecommande")
    public Date getDatecommande() {
        return datecommande;
    }

    public void setDatecommande(Date datecommande) {
        this.datecommande = datecommande;
    }

    @Basic
    @Column(name = "datelivraison")
    public Date getDatelivraison() {
        return datelivraison;
    }

    public void setDatelivraison(Date datelivraison) {
        this.datelivraison = datelivraison;
    }

    @Basic
    @Column(name = "idDev")
    public int getIdDev() {
        return idDev;
    }

    public void setIdDev(int idDev) {
        this.idDev = idDev;
    }

    @Basic
    @Column(name = "idFac")
    public Integer getIdFac() {
        return idFac;
    }

    public void setIdFac(Integer idFac) {
        this.idFac = idFac;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Commande commande = (Commande) o;
        return idCom == commande.idCom &&
                etat == commande.etat &&
                idDev == commande.idDev &&
                Objects.equals(numero, commande.numero) &&
                Objects.equals(datecommande, commande.datecommande) &&
                Objects.equals(datelivraison, commande.datelivraison) &&
                Objects.equals(idFac, commande.idFac);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idCom, numero, etat, datecommande, datelivraison, idDev, idFac);
    }

    @ManyToOne
    @PrimaryKeyJoinColumn(name = "idDev", referencedColumnName = "idDev")
    public Devis getDevisByIdDev() {
        return devisByIdDev;
    }

    public void setDevisByIdDev(Devis devisByIdDev) {
        this.devisByIdDev = devisByIdDev;
    }

    @ManyToOne
    @PrimaryKeyJoinColumn(name = "idFac", referencedColumnName = "idFac")
    public Facture getFactureByIdFac() {
        return factureByIdFac;
    }

    public void setFactureByIdFac(Facture factureByIdFac) {
        this.factureByIdFac = factureByIdFac;
    }

    @OneToMany(mappedBy = "commandeByIdCom")
    public Collection<LigneCommande> getLigneCommandesByIdCom() {
        return ligneCommandesByIdCom;
    }

    public void setLigneCommandesByIdCom(Collection<LigneCommande> ligneCommandesByIdCom) {
        this.ligneCommandesByIdCom = ligneCommandesByIdCom;
    }
}
