package com.afpa.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "ligne_devis", schema = "projet", catalog = "")
public class LigneDevis {
    private int idProd;
    private int idDev;
    private int qte;
    private int remise;
    private Produit produitByIdProd;
    private Devis devisByIdDev;

    @Id
    @Column(name = "idProd")
    public int getIdProd() {
        return idProd;
    }

    public void setIdProd(int idProd) {
        this.idProd = idProd;
    }

    @Id
    @Column(name = "idDev")
    public int getIdDev() {
        return idDev;
    }

    public void setIdDev(int idDev) {
        this.idDev = idDev;
    }

    @Basic
    @Column(name = "qte")
    public int getQte() {
        return qte;
    }

    public void setQte(int qte) {
        this.qte = qte;
    }

    @Basic
    @Column(name = "remise")
    public int getRemise() {
        return remise;
    }

    public void setRemise(int remise) {
        this.remise = remise;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LigneDevis that = (LigneDevis) o;
        return idProd == that.idProd &&
                idDev == that.idDev &&
                qte == that.qte &&
                remise == that.remise;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idProd, idDev, qte, remise);
    }

    @ManyToOne
    @PrimaryKeyJoinColumn(name = "idProd", referencedColumnName = "idProd")
    public Produit getProduitByIdProd() {
        return produitByIdProd;
    }

    public void setProduitByIdProd(Produit produitByIdProd) {
        this.produitByIdProd = produitByIdProd;
    }

    @ManyToOne
    @PrimaryKeyJoinColumn(name = "idDev", referencedColumnName = "idDev")
    public Devis getDevisByIdDev() {
        return devisByIdDev;
    }

    public void setDevisByIdDev(Devis devisByIdDev) {
        this.devisByIdDev = devisByIdDev;
    }
}
