package com.afpa.access;

import com.afpa.model.Categorie;
import com.afpa.model.Produit;
import com.google.common.hash.Hashing;
import javax.faces.view.ViewScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;


@ViewScoped
public class BdAccess implements Serializable {

   @PersistenceContext(unitName = "GestComPersistenceUnit")
    private EntityManager em;

    public BdAccess() {

    }

    public List<Produit> lireallproduit() {
        return em.createNativeQuery("select * from PRODUIT", Produit.class).getResultList();
    }

    public List<Categorie> lireallcategorie() {
        return em.createNativeQuery("select * from CATEGORIE", Categorie.class).getResultList();
    }

    @Transactional
    public void write(Produit e) {
        em.persist(e);
    }


    public EntityManager getEm() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }
}


