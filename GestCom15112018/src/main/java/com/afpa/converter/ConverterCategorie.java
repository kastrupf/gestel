package com.afpa.converter;

import com.afpa.model.Categorie;
import com.afpa.service.ServiceProduit;

import javax.enterprise.context.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

@Named
@RequestScoped
public class ConverterCategorie implements Serializable {
    @Inject
    private ServiceProduit ss;


    public Categorie getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        return ss.listeCategories().stream().filter(c->c.getLibelle().equals(s)).findFirst().orElse(null);
    }

    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Categorie categorie) {
        if(categorie==null){
            return "Toutes les catégories";
        }
        else {
            return categorie.getLibelle();
        }

    }
}